const prompt = require("prompt-sync")()
let tab = {
    "1": " ",
    "2": " ",
    "3": " ",
    "4": " ",
    "5": " ",
    "6": " ",
    "7": " ",
    "8": " ",
    "9": " "
}

let player = true
let win = true

let p1 = "x", p2 = "o"

while (win) {
    console.clear()
    affichage()
    let choice = promptCoord("Entrez votre case (1-9) : ")

    if (player) {
        tab[choice] = p1
    } else {
        tab[choice] = p2
    }

    player = !player

    checkWin()

    if(!win) {
        affichage()
        replay()
    }
}

function replay() {
    let response = promptReplay()

    if(response == "y") {
        win = true
        tab = {
            "1": " ",
            "2": " ",
            "3": " ",
            "4": " ",
            "5": " ",
            "6": " ",
            "7": " ",
            "8": " ",
            "9": " "
        }
    }
}

function promptReplay() {
    let res = ""

    while(res.toLowerCase() != "y" && res.toLowerCase() != "n"){
        res = prompt("Voulez vous rejouer ?(y/n) ")
    }

    return res.toLocaleLowerCase()
}

function promptCoord(phrase) {
    let x = NaN
    let res
    while (isNaN(x)) {
        res = prompt(phrase)
        x = parseInt(res)

        if (x < 1 || x > 9) {
            x = NaN
        } else if(tab[x] != " ") {
            x = NaN
        }
    }
    return x
}

function affichage() {
    console.log(`${tab[7]}|${tab[8]}|${tab[9]}\n${tab[4]}|${tab[5]}|${tab[6]}\n${tab[1]}|${tab[2]}|${tab[3]}\n`)
}

function checkWin() {
    let value
    for (let i = 0; i < 8; i++) {
        switch (i) {
            case 0:
                value = tab[1] + tab[2] + tab[3]
                break;
            case 1:
                value = tab[4] + tab[5] + tab[6]
                break;
            case 2:
                value = tab[7] + tab[8] + tab[9]
                break;
            case 3:
                value = tab[1] + tab[4] + tab[7]
                break;
            case 4:
                value = tab[2] + tab[5] + tab[8]
                break;
            case 5:
                value = tab[3] + tab[6] + tab[9]
                break;
            case 6:
                value = tab[1] + tab[5] + tab[9]
                break;
            case 7:
                value = tab[3] + tab[5] + tab[7]
                break;
        }

        if(value == p1+p1+p1 || value == p2+p2+p2) {
            win = false
            return console.log("Joueur " + value.slice(2) + " à gagner !")
        }
    }

    if(!Object.values(tab).includes(" ")) {
        win = false
        return console.log("Match nul")
    }
}